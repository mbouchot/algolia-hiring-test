<?php

namespace AHT\Framework\Config;

class Provider
{
    protected static $config = [];

    public static function addConfig($key, $value)
    {
        self::$config[$key] = $value;
    }

    public static function setConfig($config)
    {
        self::$config = $config;
    }

    public static function getConfig($key)
    {
        return isset(self::$config[$key]) ? self::$config[$key] : null;
    }
}