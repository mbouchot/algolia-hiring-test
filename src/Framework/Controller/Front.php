<?php

namespace AHT\Framework\Controller;

class Front
{
    protected $routes = [];

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    public function dispatch()
    {
        $request = new \AHT\Framework\Controller\Http\Request();
        $response = new \AHT\Framework\Controller\Http\Response();

        try {
            $matchedRoute = $this->route($request, $response);
            if ($matchedRoute) {
                $this->execute($matchedRoute->getAction(), $request, $response);
            } else {
                $response->addHeader('HTTP/1.1 404 Not Found');
            }
        } catch (\Exception $e) {
            $response->addHeader('HTTP/1.1 500 Internal Server Error');
            $response->setBody($e->getMessage());
        }

        $response->send();
        return $this;
    }

    protected function route(
        \AHT\Framework\Controller\Http\Request $request,
        \AHT\Framework\Controller\Http\Response $response
    ) {
        $result = null;

        foreach($this->routes as $route) {
            if ($route->match($request)
             && $route->authenticate($request)) {
                $result = $route;
            }
        }

        return $result;
    }

    protected function execute(
        \AHT\Framework\Controller\ActionInterface $action,
        \AHT\Framework\Controller\Http\Request $request,
        \AHT\Framework\Controller\Http\Response $response
    ) {
        return $action->execute($request, $response);
    }
}
