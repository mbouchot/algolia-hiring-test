<?php

namespace AHT\Framework\Controller;

interface RouteInterface
{
    public function match(\AHT\Framework\Controller\Http\Request $request);

    public function authenticate(\AHT\Framework\Controller\Http\Request $request);

    public function getAction();
}