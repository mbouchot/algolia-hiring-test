<?php

namespace AHT\Framework\Controller;

interface ActionInterface
{
    public function execute(
        \AHT\Framework\Controller\Http\Request $request,
        \AHT\Framework\Controller\Http\Response $response
    );
}