<?php

namespace AHT\Framework\Controller\Http;

class Request
{
    protected $params;

    public function addParam($key, $value)
    {
        $this->params[$key] = $value;
        return $this;
    }

    public function getParam($key)
    {
        return isset($this->params[$key]) ? $this->params[$key] : null;
    }

    public function getRequestUri()
    {
        return str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['REQUEST_URI']);
    }

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
    
    public function getQueryString($prefix = false)
    {
        return $prefix ? '?'.$_SERVER['QUERY_STRING'] : $_SERVER['QUERY_STRING'];
    }
}
