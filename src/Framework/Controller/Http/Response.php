<?php

namespace AHT\Framework\Controller\Http;

class Response
{
    protected $headers = [];

    protected $body = '';

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function addHeader($header)
    {
        $this->headers[] = $header;
        return $this;
    }

    public function send()
    {
        foreach ($this->headers as $header) {
            header($header);
        }

        print $this->body;
    }
}