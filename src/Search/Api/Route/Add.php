<?php

namespace AHT\Search\Api\Route;

class Add implements \AHT\Framework\Controller\RouteInterface
{
    protected $path = '/api/1/apps';

    public function authenticate(\AHT\Framework\Controller\Http\Request $request)
    {
        return true;
    }

    public function getAction()
    {
        return new \AHT\Search\Api\Action\Add();
    }

    public function match(\AHT\Framework\Controller\Http\Request $request)
    {
        if ($request->getMethod() != 'POST') {
            return false;
        }

        if (strpos($request->getRequestUri(), $this->path) === 0) {
            return true;
        }

        return false;
    }
}