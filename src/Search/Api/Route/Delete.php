<?php

namespace AHT\Search\Api\Route;

class Delete implements \AHT\Framework\Controller\RouteInterface
{
    protected $path = '/api/1/apps/:id';

    public function authenticate(\AHT\Framework\Controller\Http\Request $request)
    {
        return true;
    }

    public function getAction()
    {
        return new \AHT\Search\Api\Action\Delete();
    }

    public function match(\AHT\Framework\Controller\Http\Request $request)
    {
        if ($request->getMethod() != 'DELETE') {
            return false;
        }

        if (strpos($request->getRequestUri(), str_replace('/:id', '', $this->path)) === 0) {
            $request->addParam('id', str_replace('/api/1/apps/', '', $request->getRequestUri()));
            return true;
        }

        return false;
    }
}