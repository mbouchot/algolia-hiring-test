<?php

namespace AHT\Search\Api\Action;

class Add implements \AHT\Framework\Controller\ActionInterface
{
    public function execute(
        \AHT\Framework\Controller\Http\Request $request,
        \AHT\Framework\Controller\Http\Response $response
    ) {
        try {
            $jsonData = json_decode(file_get_contents("php://input"), true);

            if ($jsonData === null) {
                throw new \Exception('Invalid data.');
            }

            $searchEngineAlgolia = new \AHT\Search\Model\SearchEngine\Algolia();
            $result = $searchEngineAlgolia->index(
                [$jsonData],
                \AHT\Framework\Config\Provider::getConfig('index')
            );

            $response->addHeader('Content-Type: application/json');
            $response->setBody(json_encode(['id' => $result[0]]));
        } catch (\Exception $e) {
            $response->addHeader('HTTP/1.1 500 Internal Server Error');
            $response->setBody($e->getMessage());
        }
    }

}