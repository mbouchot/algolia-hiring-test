<?php

namespace AHT\Search\Api\Action;

class Delete implements \AHT\Framework\Controller\ActionInterface
{
    public function execute(
        \AHT\Framework\Controller\Http\Request $request, 
        \AHT\Framework\Controller\Http\Response $response
    ) {
        try {
            $id = $request->getParam('id');

            if ($id === null) {
                throw new \Exception('Invalid data.');
            }

            $searchEngineAlgolia = new \AHT\Search\Model\SearchEngine\Algolia();
            $searchEngineAlgolia->deleteByIds(
                [$id],
                \AHT\Framework\Config\Provider::getConfig('index')
            );
        } catch (\Exception $e) {
            $response->addHeader('HTTP/1.1 500 Internal Server Error');
            $response->setBody($e->getMessage());
        }
    }

}