<?php

namespace AHT\Search\Controller\Action;

class Index implements \AHT\Framework\Controller\ActionInterface
{
    public function execute(
        \AHT\Framework\Controller\Http\Request $request,
        \AHT\Framework\Controller\Http\Response $response
    ) {
        try {
            $config = [
                'app_id' => \AHT\Framework\Config\Provider::getConfig('app_id'),
                'public_api_key' => \AHT\Framework\Config\Provider::getConfig('public_api_key'),
            ];
            include __DIR__ . '/../../View/main.php';
        } catch (\Exception $e) {
            $response->addHeader('HTTP/1.1 500 Internal Server Error');
            $response->setBody($e->getMessage());
        }
    }

}