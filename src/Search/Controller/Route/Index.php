<?php

namespace AHT\Search\Controller\Route;

class Index implements \AHT\Framework\Controller\RouteInterface
{
    protected $path = '';

    public function authenticate(\AHT\Framework\Controller\Http\Request $request)
    {
        return true;
    }

    public function getAction()
    {
        return new \AHT\Search\Controller\Action\Index();
    }

    public function match(\AHT\Framework\Controller\Http\Request $request)
    {
        if ($request->getMethod() != 'GET') {
            return false;
        }

        $requestUri = trim(str_replace($request->getQueryString(true), '', $request->getRequestUri()), '/');

        if ($requestUri == $this->path) {
            return true;
        }

        return false;
    }
}