<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Algolia Hiring Test</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/instantsearch.js@2.10.4/dist/instantsearch.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/instantsearch.js@2.10.4/dist/instantsearch-theme-algolia.min.css">
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@2.10.4"></script>
</head>

<body>
    <header class="container-fluid">
        
        <div class="row">
            <div class="col-sm-9">
                <h1>Algolia Hiring Test</h1>
            </div>
            <div class="col-sm-3">
                <div id="searchbox"></div>
            </div>
        </div>
    
    </header>

    <main class="container-fluid">

        <div class="row">
            <div class="col-sm-2">
                <div id="refinement-list"></div>
            </div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="sort-by"></div>
                    </div>
                </div>
                <div class="border-top my-3"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="hits"></div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <script type="text/javascript">
        var search = instantsearch({
            appId: '<?php echo $config['app_id'] ?>',
            apiKey: '<?php echo $config['public_api_key'] ?>',
            indexName: 'algolia_hiring_test',
            routing: true
        });

        search.addWidget(
            instantsearch.widgets.refinementList({
                container: '#refinement-list',
                attributeName: 'category',
                templates: {
                    header: 'Category'
                }
            })
        );

        search.addWidget(
            instantsearch.widgets.sortBySelector({
                container: '#sort-by',
                indices: [
                    {'name': 'algolia_hiring_test', 'label': 'Most Relevant'},
                    {'name': 'algolia_hiring_test_rank_asc', 'label': 'Lowest Rank'},
                    {'name': 'algolia_hiring_test_rank_desc', 'label': 'Highest Rank'}
                ]
            })
        );

        search.addWidget(
            instantsearch.widgets.searchBox({
                container: '#searchbox',
                placeholder: 'Search for products'
            })
        );

        search.addWidget(
            instantsearch.widgets.hits({
                container: '#hits',
                templates: {
                    empty: 'No results',
                    item:   '<article class="hit">'+
                                '<div class="hit-image-container">'+
                                    '<a href="{{link}}"><img src="https://via.placeholder.com/300" alt="{{name}}" class="hit-image"></a>'+
                                '</div>'+
                                '<div class="hit-info-container">'+
                                    '<h1 class="hit-name">{{name}}</h1>'+
                                    '<p class="hit-category">{{category}}</p>'+
                                '</div>'+
                            '</article>'
                },
            })
        );

        search.start();
    </script>

</body>

</html>
