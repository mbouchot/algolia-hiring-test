<?php

namespace AHT\Search\Model;

interface SearchEngineInterface
{
    public function index($documents, $index);
    
    public function deleteByIds($documentIds, $index);
}