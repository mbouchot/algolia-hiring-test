<?php

namespace AHT\Search\Model\SearchEngine;

class Algolia implements \AHT\Search\Model\SearchEngineInterface
{
    protected function getClient()
    {
        return \Algolia\AlgoliaSearch\SearchClient::create(
            \AHT\Framework\Config\Provider::getConfig('app_id'),
            \AHT\Framework\Config\Provider::getConfig('private_api_key')
        );
    }

    public function deleteByIds($documentIds, $index)
    {
        $this->getClient()
            ->initIndex($index)
            ->deleteObjects($documentIds);
    }

    public function index($documents, $index)
    {
        $response = $this->getClient()
            ->initIndex($index)
            ->saveObjects($documents, [
                'autoGenerateObjectIDIfNotExist' => true
            ]);
        
        return $response->current()['objectIDs'];
    }
}