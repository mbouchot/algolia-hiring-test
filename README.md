# Algolia Hiring Test

Algolia Hiring Test project is my delivery for [the technical test](https://gist.github.com/rayrutjes/3896136f9a7c37b9e9049fbc60c6d4ae)

## Usage

[Demo Web page](http://188.165.193.10:8109) : http://188.165.193.10:8109

Endpoint to add a message is : 
[POST] http://188.165.193.10:8109/api/1/apps
A curl sample :

```sh
curl -X POST -i 'http://188.165.193.10:8109/api/1/apps' --data '{
    "name": "Test3",
    "image": "http://a3.mzstatic.com/us/r1000/090/Purple/v4/20/bd/a2/20bda225-6144-cb99-46ef-d0fc15fc456a/mzl.okdjewbf.175x175-75.jpg",
    "link": "http://itunes.apple.com/us/app/ibooks/id364709193?mt=8",
    "category": "Books",
    "rank": 1
}'
```

Endpoint to delete a message is : 
[DELETE] http://188.165.193.10:8109/api/1/apps/:id
A curl sample :

```sh
curl -X DELETE -i 'http://188.165.193.10:8109/api/1/apps/327413010'
```

## Tasks done to realize that project

1. Read the exercise carefully
2. Create a sample test account in algolia, my credentials are stored in bootstrap.php file
3. Find a backend library for algolia : [Algolia PHP Client](https://github.com/algolia/algoliasearch-client-php>)
4. Create the backend App
5. Read the doc for : [instantsearch.js (v2)](https://community.algolia.com/instantsearch.js/)
6. Configure the indexes in Algolia Back Office for the frontend App
7. Create the Frontend App 

## From my understanding of the exercise

For the backend part it was said that i must build a simple MVC framework, 
in other cases for this kind of project i'd rather use [Silex](https://silex.symfony.com/),
but here i've built this app as a 'modular' app with 2 folders :

 - One for the Framework
 - One for the Search Part

In this case we should add another modules : (admin, cms, ....)

For the frontend part, the main difficulty is the UI as it is costly to implement, for this 
reason, i've made the choice to focus on the layout to have a decent render even it is not 'really sexy',
and worked a little on responsive layout part.

## Possible Optimizations

 - Handle IDL/Documentation/Authentication for API (Swagger could be provided)
 - Address XSS issues if the content contains htmlentities
 - Implement a DI on this project in order to build easily unit tests
