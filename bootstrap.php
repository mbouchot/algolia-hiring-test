<?php

require __DIR__.'/vendor/autoload.php';
date_default_timezone_set('UTC');

\AHT\Framework\Config\Provider::setConfig([
    'app_id' => 'BTQRM5A2BE',
    'private_api_key' => 'ed43ed5087e45bbb9b58d23a14bf4ce0',
    'public_api_key' => '1b80e625d7d704d9c2d246710590bb65',
    'index' => 'algolia_hiring_test',
]);

$front = new \AHT\Framework\Controller\Front([
    new \AHT\Search\Api\Route\Add(),
    new \AHT\Search\Api\Route\Delete(),
    new \AHT\Search\Controller\Route\Index()
]);

return $front;