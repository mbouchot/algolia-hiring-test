<?php

require __DIR__.'/../bootstrap.php';
$importFile = __DIR__.'/../resources/data.json';

$jsonData = json_decode(file_get_contents($importFile), true);

$searchEngineAlgolia = new \AHT\Search\Model\SearchEngine\Algolia();
$searchEngineAlgolia->index(
    $jsonData,
    \AHT\Framework\Config\Provider::getConfig('index')
);